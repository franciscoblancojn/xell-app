import { RouterContext } from "next/dist/shared/lib/router-context";

import "@/styles/style.css";

import { GlobalNotification } from "@/hook/useNotification";
import { Root } from "@/components/Root";
import { Theme } from "@/config/theme";

import { withGlobals } from "@luigiminardim/storybook-addon-globals-controls";

export const globalTypes = {
    Theme: {
        name: "Theme",
        defaultValue: Theme.styleTemplate,
        control: {
            type: "select",
            options: ["_default", "tolinkme", "mooveri"],
        },
    },
};

export const parameters = {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/,
        },
    },
    nextRouter: {
        Provider: RouterContext.Provider,
        path: "/", // defaults to `/`
        asPath: "/", // defaults to `/`
        query: {}, // defaults to `{}`
        push() {},
        isReady: true,
        isStorybook: true,
    },
};

export const decorators = [
    withGlobals((Story, globalValues) => (
        <>
            <Story />
            <GlobalNotification />
            <Root styleTemplate={globalValues.Theme} />
        </>
    )),
];
