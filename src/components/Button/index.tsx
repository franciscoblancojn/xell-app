import {
  Button as ButtonTemplate,
  ButtonBaseProps,
} from 'fenextjs-component/cjs/Button';
import { ButtonStyled } from '@/components/Button/styled';


export const Button = (props: ButtonBaseProps) => {
  return (
    <ButtonStyled>
      <ButtonTemplate {...props} />
    </ButtonStyled>
  );
};
